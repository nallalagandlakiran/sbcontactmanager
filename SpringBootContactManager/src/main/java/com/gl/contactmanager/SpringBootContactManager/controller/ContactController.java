package com.gl.contactmanager.SpringBootContactManager.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.contactmanager.SpringBootContactManager.dao.ContactService;
import com.gl.contactmanager.SpringBootContactManager.dao.UserService;
import com.gl.contactmanager.SpringBootContactManager.model.Contact;
import com.gl.contactmanager.SpringBootContactManager.model.User;

@Controller
@RequestMapping("/user/contact")
public class ContactController {

	@Autowired
	private ContactService contactService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/contactpage")
	public ModelAndView getContactPaqe() {
		return new ModelAndView("UserContactForm", "contactObject", new Contact());
	}
	
	@RequestMapping("/saveContact")
	public ModelAndView saveContact(@ModelAttribute("contactObject")Contact contact, HttpSession session) {
		User loginUser = ((User)session.getAttribute("loginnedUser"));
		User user = userService.getById(loginUser.getUserId());
		contact.setUser(user);
		System.out.println(loginUser.getEmailId());
		System.out.println(loginUser.getUsername());
		contactService.saveContact(contact);
		return new ModelAndView("redirect:/user/dashboard");
	}
	
	@RequestMapping("/deletecontact/{contactId}")
	public ModelAndView deleteContact(@PathVariable("contactId")int contactId) {
		System.out.println(contactId);
		contactService.deleteContact(contactId);
		return new ModelAndView("forward:/user/dashboard");
	}
	
	@RequestMapping("/updatecontact/{contactId}")
	public ModelAndView updateContactForm(@PathVariable("contactId")int contactId) {
		Contact contact = contactService.getContactById(contactId);
		return new ModelAndView("ContactUpdateForm", "contactObject", contact);
	}
	
	@RequestMapping("/update")
	public ModelAndView updateContact(@ModelAttribute("contactObject")Contact contact) {
		contactService.saveContact(contact);
		return new ModelAndView("forward:/user/dashboard");
	}
}
