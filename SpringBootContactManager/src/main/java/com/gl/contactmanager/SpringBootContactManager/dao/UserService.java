package com.gl.contactmanager.SpringBootContactManager.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gl.contactmanager.SpringBootContactManager.model.User;
import com.gl.contactmanager.SpringBootContactManager.reposotories.UserRepositories;

@Component
public class UserService {

	@Autowired
	private UserRepositories repositories;

	public User save(User user) {
		return repositories.save(user);
	}
	
	public User getById(int id) {
		return repositories.getById(id);
	}
	
	public User loginUser(User user) {
		return repositories.findByUsernameAndPassword(user.getUsername(), user.getPassword());
	}
}
