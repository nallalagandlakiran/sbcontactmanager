package com.gl.contactmanager.SpringBootContactManager;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootContactManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootContactManagerApplication.class, args);
	}

}
