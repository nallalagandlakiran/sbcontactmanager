package com.gl.contactmanager.SpringBootContactManager.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.contactmanager.SpringBootContactManager.dao.ContactService;
import com.gl.contactmanager.SpringBootContactManager.dao.UserService;
import com.gl.contactmanager.SpringBootContactManager.model.Contact;
import com.gl.contactmanager.SpringBootContactManager.model.User;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ContactService contactService;

	@RequestMapping("/register")
	public ModelAndView getRegisterPage(@ModelAttribute("registerErrorMessage") String errorMessage) {
		ModelAndView modelAndView = new ModelAndView("UserRegisterForm", "userObject", new User());
		modelAndView.addObject("registerErrorMessage", errorMessage);
		return modelAndView;
	}

	@RequestMapping("/registration")
	public ModelAndView registerUserDetails(@ModelAttribute("userObject") User user, HttpSession session) {
		User registerUser = userService.save(user);
		if (registerUser != null) {
			return new ModelAndView("redirect:/");
		} else {
			return new ModelAndView("redirect:/", "registerErrorMessage",
					"username already exsisted try another details");
		}
	}
	
	@RequestMapping("/loginpage")
	public ModelAndView getLoginPage(@ModelAttribute("loginErrorMessage")String errorMessage) {
		ModelAndView modelAndView = new ModelAndView("UserLoginForm", "userObject", new User());
		modelAndView.addObject("registerErrorMessage", errorMessage);
		return modelAndView;
	}
	
	@RequestMapping("/login")
	public ModelAndView loginUser(@ModelAttribute("userObject")User user, HttpSession session) {
		User loginUser = userService.loginUser(user);
		if(loginUser != null) {
			session.setAttribute("loginnedUser", loginUser);
			return new ModelAndView("redirect:/user/dashboard");
		}else {
			return new ModelAndView("redirect:/user/loginpage", "loginErrorMessage", "Invalid username or password");
		}
	}
	
	@RequestMapping("/dashboard")
	public ModelAndView getDashboard(HttpSession session, HttpServletRequest request) {
		User user = (User) request.getSession(false).getAttribute("loginnedUser");
		System.out.println(user.getUserId());
		List<Contact> contacts = contactService.getAllContacts(user);
//		System.out.println("------------------");
//		System.out.println(contacts.get(0));
		ModelAndView modelAndView = new ModelAndView("UserDashboard", "loginnedUser", user);
		modelAndView.addObject("contacts", contacts);
		return modelAndView;
	}
}
