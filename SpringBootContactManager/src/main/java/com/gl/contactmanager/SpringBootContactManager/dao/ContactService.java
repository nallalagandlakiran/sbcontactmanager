package com.gl.contactmanager.SpringBootContactManager.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gl.contactmanager.SpringBootContactManager.model.Contact;
import com.gl.contactmanager.SpringBootContactManager.model.User;
import com.gl.contactmanager.SpringBootContactManager.reposotories.ContactRepositories;

@Component
public class ContactService {

	@Autowired
	private ContactRepositories contactRepositories;
	
	public List<Contact> getAllContacts(User user){
		List<Contact> contacts = contactRepositories.findAll();
		List<Contact> filtered = contacts.stream().filter( c -> c!=null && c.getUser().getUserId() == user.getUserId()).collect(Collectors.toList());
		return filtered.isEmpty() ? null : filtered;
	}
	
	public Contact saveContact(Contact contact) {
		return contactRepositories.saveAndFlush(contact);
	}
	
	public void deleteContact(int contactId) {
		contactRepositories.deleteById(contactId);
	}
	
	public Contact getContactById(int contactId) {
		return contactRepositories.getById(contactId);
	}
	
}
