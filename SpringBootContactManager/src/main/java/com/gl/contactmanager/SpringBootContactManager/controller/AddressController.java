package com.gl.contactmanager.SpringBootContactManager.controller;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.contactmanager.SpringBootContactManager.model.Address;
import com.gl.contactmanager.SpringBootContactManager.model.User;
import com.gl.contactmanager.SpringBootContactManager.reposotories.UserRepositories;

@Controller
@RequestMapping("/user/address")
public class AddressController {

	@Autowired
	private UserRepositories userRepositories;
	
	@RequestMapping("/addresspage")
	public ModelAndView getAddressForm(HttpSession session) {
		User loginUser = ((User)session.getAttribute("loginnedUser"));
		Address address = loginUser.getAddress();
		if(address == null) {			
			return new ModelAndView("UserAddressForm", "addressObject", new Address());
		}else {
			Optional<User> user = userRepositories.findById(loginUser.getUserId());
			address = user.get().getAddress();
			return new ModelAndView("UserAddressForm", "addressObject", address);
		}
	}
	
	@RequestMapping("/updateAddress")
	public ModelAndView updateAdress(@ModelAttribute("addressObject")Address address, HttpSession session) {
		User loginUser = ((User)session.getAttribute("loginnedUser"));
		loginUser.setAddress(address);
		userRepositories.save(loginUser);
		return new ModelAndView("redirect:/user/dashboard");
	}
}
