package com.gl.contactmanager.SpringBootContactManager.reposotories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.contactmanager.SpringBootContactManager.model.Contact;

public interface ContactRepositories extends JpaRepository<Contact, Integer>{
}
