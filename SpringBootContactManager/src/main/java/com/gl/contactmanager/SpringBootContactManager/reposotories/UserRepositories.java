package com.gl.contactmanager.SpringBootContactManager.reposotories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.contactmanager.SpringBootContactManager.model.User;

public interface UserRepositories extends JpaRepository<User, Integer>{

	public User findByUsernameAndPassword(String username, String password);
}
