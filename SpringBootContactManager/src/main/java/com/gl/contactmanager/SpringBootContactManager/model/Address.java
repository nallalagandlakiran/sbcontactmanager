package com.gl.contactmanager.SpringBootContactManager.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int addressId;
	private String faltNumber;
	private String area;
	private String city;
	private String zipcode;

	public Address() {
	}

	public Address(String faltNUmber, String area, String city, String zipcode) {
		super();
		this.faltNumber = faltNUmber;
		this.area = area;
		this.city = city;
		this.zipcode = zipcode;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getFaltNumber() {
		return faltNumber;
	}

	public void setFaltNumber(String faltNumber) {
		this.faltNumber = faltNumber;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}
