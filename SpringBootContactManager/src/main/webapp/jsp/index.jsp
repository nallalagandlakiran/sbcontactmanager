<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
    <style>
        body{
            margin: 0px;
            padding: 0px;
            height: 100%;
            background: linear-gradient(270deg, #8aa19a 0%, #a18a91 100%);
        }
        .Homepage-content{
            max-width: 50%;
            margin-top: 25vh;
            margin-left: auto;
            margin-right: auto;
            display: flex;
            justify-content: center;
            flex-direction: column;
            text-align: center;
        }
        input{
            height: 35px;
            width: 150px;
            text-align: center;
            border: none;
            border-radius: 20px;
            background-color: #275efe;
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
            color: white;
        }
        
    </style>
</head>
<body>
    <div class="Homepage-content">
        <h1>Welcome to the Contact Manager</h1>
        <h3>Save your contacts here</h3>
        <div>
            <input type="button" value="Login" onclick="window.location.href='user/loginpage'" />
            <input type="button" value="Register User" onclick="window.location.href='user/register'" />
        </div>
    </div>
</body>
</html>