<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<html>
<head>
<title>Spring MVC Form Handling</title>
</head>

<body>
	<h2>Registration Form</h2>
	<form:form method="POST" action="/user/contact/update"
		modelAttribute="contactObject">
		<table>
			<tr>
				<td><form:label path="firstName">firstname</form:label></td>
				<td><form:input path="firstName" /></td>
			</tr>
			<tr>
				<td><form:label path="lastName">lastname</form:label></td>
				<td><form:input path="lastName" /></td>
			</tr>
			<tr>
				<td><form:label path="mobileNumber">mobile number</form:label></td>
				<td><form:input path="mobileNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="officeNumber">office number</form:label></td>
				<td><form:input path="officeNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="emailId">emailId</form:label></td>
				<td><form:input path="emailId" /></td>
			</tr>
			<tr hidden>
				<td><form:label path="contactId">contactId</form:label></td>
				<td><form:input path="contactId" /></td>
			</tr>
			<tr hidden>
				<td ><form:label path="user">user</form:label></td>
				<td><form:input path="user" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>