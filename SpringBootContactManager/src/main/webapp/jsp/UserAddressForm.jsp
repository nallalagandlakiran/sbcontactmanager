<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<html>
<head>
<title>Spring MVC Form Handling</title>
</head>

<body>
	<h2>Address Form</h2>
	<form:form method="POST" action="updateAddress"
		modelAttribute="addressObject">
		<table>
			<tr hidden>
				<td><form:label path="addressId">addressid</form:label></td>
				<td><form:input path="addressId" /></td>
			</tr>
			<tr>
				<td><form:label path="faltNumber">Flat No</form:label></td>
				<td><form:input path="faltNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="area">Area</form:label></td>
				<td><form:input path="area" /></td>
			</tr>
			<tr>
				<td><form:label path="city">city</form:label></td>
				<td><form:input path="city" /></td>
			</tr>
			<tr>
				<td><form:label path="zipcode">Zip Code</form:label></td>
				<td><form:input path="zipcode" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>
