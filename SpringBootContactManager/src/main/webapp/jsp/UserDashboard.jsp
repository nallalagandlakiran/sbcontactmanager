<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.Navbar {
	max-width: 90%;
	background-color: fuchsia;
	padding: 10px;
	margin: auto;
	border-radius: 20px;
	display: flex;
	flex-direction: column;
}

.Nav-items {
	text-align: center;
}

.Operation-container {
	display: flex;
	flex-direction: column;
	justify-content: center;
	max-width: 60%;
	margin: auto;
	margin-top: 5em;
}

.Operation-links {
	text-align: center;
	padding: 20px;
}

input {
	height: 35px;
	width: 150px;
	text-align: center;
	border: none;
	border-radius: 20px;
	background-color: #275efe;
	font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande',
		'Lucida Sans', Arial, sans-serif;
	color: white;
}

.subcontent {
	display: flex;
	max-width: 90%; margin : 20px auto; max-width : 90%; margin : 20px auto;
	justify-content: space-between;
	margin: 20px auto;
	max-width: 90%;
	margin: 20px auto;
}
</style>
</head>
<body>
	<div class="Navbar">
		<div class="Nav-items">
			<h2 style="color: white;">Welcome ${loginnedUser.username}</h2>
		</div>
		<div class="Nav-items">
			<input style="justify-content: center;" type="button" value="Logout"
				onclick="window.location.href='logout'" />
		</div>
	</div>

	<div class="subcontent">
		<input
			style="color: white; height: 35px; border-radius: 10px; border: none; background-color: #275efe;"
			type="button" onclick="window.location.href = '/user/contact/contactpage' "
			value="Add new Contact" /> <input
			style="color: white; height: 35px; border-radius: 10px; border: none; background-color: #275efe;"
			type="button" onclick="window.location.href = '/user/address/addresspage' "
			value="Update Address" />
	</div>

	<center>
		<h1>Contacts</h1>
		<table style="width: 90%" border="1">
			<tr>
				<th style="width: 20%">firstName</th>
				<th style="width: 20%">lastname</th>
				<th style="width: 20%">Mobile Number</th>
				<th style="width: 20%">Office Number</th>
				<th style="width: 20%">Email Id</th>
				<th style="width: 20%">Options</th>
			</tr>
			<c:if test="${not empty contacts}">
				<c:forEach items="${contacts}" var="contact">
					<tr>
						<td>${contact.firstName}</td>
						<td>${contact.lastName}</td>
						<td>${contact.mobileNumber}</td>
						<td>${contact.officeNumber}</td>
						<td>${contact.emailId}</td>
						<td><a href="/user/contact/updatecontact/${contact.contactId}">Edit</a> <a
							href="/user/contact/deletecontact/${contact.contactId}"
							onclick="return confirm('Do you want to delete? ')">Delete</a></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
	</center>
</body>
</html>