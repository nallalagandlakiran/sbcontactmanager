<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<html>
<head>
<title>Spring MVC Form Handling</title>
</head>

<body>
	<h2>Registration Form</h2>
	<form:form method="POST" action="saveContact"
		modelAttribute="contactObject">
		<table>
			<tr>
				<td><form:label path="firstName">firstname</form:label></td>
				<td><form:input path="firstName" /></td>
			</tr>
			<tr>
				<td><form:label path="lastName">lastname</form:label></td>
				<td><form:input path="lastName" /></td>
			</tr>
			<tr>
				<td><form:label path="mobileNumber">mobile number</form:label></td>
				<td><form:input path="mobileNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="officeNumber">office number</form:label></td>
				<td><form:input path="officeNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="emailId">emailId</form:label></td>
				<td><form:input path="emailId" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
	<h2>${registerErrorMessage}</h2>
</body>
</html>
